# CSE 6230, Fall 2014, Lab 5: Tuning GPU reduction #

For instructions, see: https://bitbucket.org/gtcse6230fa14/lab5/wiki/Home

This lab is due before class on Thursday, October 9, 2014.

All 7 versions of reduce coded. Required performance achieved for each case. Peak BW of 108.7 Gbps achieved for the last version. 
One deviation from the suggestion in the slides, I have made was to use #pragma unroll for unrolling the expressions instead of writing them down.
